﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S10207327_MyBankApp
{
    class SavingsAccount: BankAccount
    {
        public double Rate { get; set; }
        public SavingsAccount() : base() { }
        public SavingsAccount(string accno, string accname, double bal, double r): base(accno, accname, bal) { Rate = r; }
        public double CalculateInterest()
        {
            return (Balance * Rate / 100);
        }
        public override string ToString()
        {
            return base.ToString() + "\tInterest Rate:" + Rate;
        }
    }
    
}
