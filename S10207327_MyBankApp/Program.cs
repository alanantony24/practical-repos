﻿using System;
using System.Collections.Generic;
using System.IO;
namespace S10207327_MyBankApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<SavingsAccount> savingsList = new List<SavingsAccount>();
            InitialiseList(savingsList);
            DisplayMenu();
            Console.Write("Enter your Option : ");
            string op = Console.ReadLine();
            if (op == "1")
            {
                DisplayAll(savingsList);
            }
            else if (op == "2")
            {
                Console.Write("Enter Acc No too search for: ");
                string accno = Console.ReadLine();
                SearchAcc(savingsList, accno);
                if (SearchAcc(savingsList, accno) != null)
                {
                    Console.Write("Enter amount to deposit: ");
                    double amount = Convert.ToDouble(Console.ReadLine());
                    SavingsAccount ob = SearchAcc(savingsList, accno);
                    ob.Deposit(amount);
                    Console.WriteLine("{0} deposited successfully!", amount);
                    Console.WriteLine("Acc No: {0} Acc Name: {1} Balance: {2} Rate: {3}", ob.AccNo, ob.AccName, ob.Balance, ob.Rate);
                }
                else
                    Console.WriteLine("Unable to find the account!");
            }
            else if (op == "3")
            {
                Console.Write("Enter the account number: ");
                string accno = Console.ReadLine();
                if(SearchAcc(savingsList, accno) != null)
                {
                    Console.Write("Enter Amount to withdraw: ");
                    double amount = Convert.ToDouble(Console.ReadLine());
                    SavingsAccount ob = SearchAcc(savingsList, accno);
                    if(ob.Balance > amount)
                    {
                        ob.Withdraw(amount);
                        Console.WriteLine("${0} withdrawn successfully!", amount);
                        Console.WriteLine("Acc No: {0} Acc Name: {1} Balance: {2} Rate: {3}", ob.AccNo, ob.AccName, ob.Balance, ob.Rate);
                    }else if(amount > ob.Balance)
                    {
                        Console.WriteLine("Insufficient Funds!");
                    }

                }
                else
                {
                    Console.WriteLine("Unable to find account number! Please try again");
                }

            }
            else if(op == "0")
            {
                Console.WriteLine("----------\nGoodbye!\n----------");
            }
        }
        static void InitialiseList(List<SavingsAccount> savingsAccount)
        {
            string[] csvlines = File.ReadAllLines("savings_account.csv");
            for (int i = 1; i < csvlines.Length; i++)
            {
                string[] data = csvlines[i].Split(",");
                SavingsAccount newSavingsAccount = new SavingsAccount(data[0], data[1], Convert.ToDouble(data[2]), Convert.ToDouble(data[3]));
                savingsAccount.Add(newSavingsAccount);
            }
        }
        static void DisplayMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("[1] Display All Accounts");
            Console.WriteLine("[2] Deposit");
            Console.WriteLine("[3] Withdraw");
            Console.WriteLine("[0] Exit");
        }
        static void DisplayAll(List<SavingsAccount> sList)
        {
            for (int i = 0; i < sList.Count; i++)
            {
                Console.WriteLine(sList[i]);
            }
        }
        static SavingsAccount SearchAcc(List<SavingsAccount> sList, string accNo)
        {
            for (int i = 0; i < sList.Count; i++)
            {
                if (accNo == sList[i].AccNo)
                    return sList[i];
            }
            return null;
        }

    }
}
