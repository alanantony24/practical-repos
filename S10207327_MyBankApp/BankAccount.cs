﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S10207327_MyBankApp
{
    class BankAccount
    {
        public string AccNo { get; set; }
        public string AccName { get; set; }
        public double Balance { get; set; }

        public BankAccount() {}
        public BankAccount(string accno, string accname, double bal)
        {
            AccNo = accno;
            AccName = accname;
            Balance = bal;
        }
        public void Deposit(double depositamt)
        {
            Balance = Balance + depositamt;
        }
        public bool Withdraw(double withdrawamt)
        {
            if (withdrawamt < Balance)
            {
                Balance = Balance - withdrawamt;
                return true;
            }
            else
                return false;
        }
        public override string ToString()
        {
            return "AccountNo:" + AccNo +
                "\tAccName:" + AccName +
                "\tBalance:" + Balance;
        }
    }
}
